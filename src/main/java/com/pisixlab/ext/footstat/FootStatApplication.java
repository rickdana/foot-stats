package com.pisixlab.ext.footstat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootStatApplication {

	public static void main(String[] args) {
		SpringApplication.run(FootStatApplication.class, args);
	}
}
